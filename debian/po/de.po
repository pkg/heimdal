# translation of heimdal_0.7.2.dfsg.1-11_de.po to German
#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans#
#    Developers do not need to manually edit POT or PO files.
#
# Erik Schanze <eriks@debian.org>, 2004-2007.
msgid ""
msgstr ""
"Project-Id-Version: heimdal_0.7.2.dfsg.1-11_de\n"
"Report-Msgid-Bugs-To: heimdal@packages.debian.org\n"
"POT-Creation-Date: 2011-08-04 16:42+0200\n"
"PO-Revision-Date: 2007-10-05 22:39+0200\n"
"Last-Translator: Erik Schanze <eriks@debian.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"

#. Type: string
#. Description
#: ../heimdal-kdc.templates:1001
msgid "Local realm name:"
msgstr "Ihr lokaler Realm-Name:"

#. Type: string
#. Description
#: ../heimdal-kdc.templates:1001
msgid "Please enter the name of the local Kerberos realm."
msgstr "Bitte geben Sie den Namen des lokalen Kerberos-Realms ein."

#. Type: string
#. Description
#: ../heimdal-kdc.templates:1001
msgid ""
"Using the uppercase domain name is common. For instance, if the host name is "
"host.example.org, then the realm will become EXAMPLE.ORG. The default for "
"this host is ${default_realm}."
msgstr ""
"Es ist üblich, den großgeschriebenen Domänennamen zu verwenden. Wenn z. B. "
"der Rechnername »host.example.org« lautet, dann ist Ihr Realm-Name »EXAMPLE."
"ORG«. Die Standardeinstellung für diesen Rechner ist ${default_realm}."

#. Type: password
#. Description
#: ../heimdal-kdc.templates:2001
msgid "KDC password:"
msgstr "KDC-Passwort:"

#. Type: password
#. Description
#: ../heimdal-kdc.templates:2001
msgid ""
"Heimdal can encrypt the key distribution center (KDC) data with a password. "
"A hashed representation of this password will be stored in /var/lib/heimdal-"
"kdc/m-key."
msgstr ""
"Heimdal kann die Daten des zentralen Schlüsselverteilers (key distribution "
"center - KDC) mit einem Passwort verschlüsseln. Ein Hash-Wert davon wird in "
"der Datei /var/lib/heimdal-kdc/m-key abgelegt."
