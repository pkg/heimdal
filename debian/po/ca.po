# heimdal po-debconf translation to Catalan
# Copyright (C) 2016 Software in the Public Interest
# This file is distributed under the same license as the heimdal package.
#
# Sandra Pastrana <speix@openmailbox.org>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: 1.7~git20160515+dfsg-1\n"
"Report-Msgid-Bugs-To: heimdal@packages.debian.org\n"
"POT-Creation-Date: 2011-08-04 16:42+0200\n"
"PO-Revision-Date: 2016-06-13 15:59+0200\n"
"Last-Translator: Sandra Pastrana <speix@openmailbox.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.5\n"

#. Type: string
#. Description
#: ../heimdal-kdc.templates:1001
msgid "Local realm name:"
msgstr "Nom del regne local:"

#. Type: string
#. Description
#: ../heimdal-kdc.templates:1001
msgid "Please enter the name of the local Kerberos realm."
msgstr "Si us plau, introdueixi el nom del regne local de Kerberos."

#. Type: string
#. Description
#: ../heimdal-kdc.templates:1001
msgid ""
"Using the uppercase domain name is common. For instance, if the host name is "
"host.example.org, then the realm will become EXAMPLE.ORG. The default for "
"this host is ${default_realm}."
msgstr ""
"És comú utilitzar el nom de domini en majúscules. Per exemple, si el nom de "
"la màquina és host.exemple.org, aleshores el regne serà EXEMPLE.ORG. El "
"predeterminat per aquesta màquina és ${default_realm}."

#. Type: password
#. Description
#: ../heimdal-kdc.templates:2001
msgid "KDC password:"
msgstr "Contrasenya de KDC:"

#. Type: password
#. Description
#: ../heimdal-kdc.templates:2001
msgid ""
"Heimdal can encrypt the key distribution center (KDC) data with a password. "
"A hashed representation of this password will be stored in /var/lib/heimdal-"
"kdc/m-key."
msgstr ""
"El Heimdal pot xifrar les dades del centre de distribució de claus (KDC) amb "
"una contrasenya. El resultat d'una funció resum d'aquesta contrasenya "
"s'emmagatzemarà a /var/lib/heimdal-kdc/m-key."

