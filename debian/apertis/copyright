Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 1995-2016, Kungliga Tekniska Högskolan
License: BSD-3-clause

Files: NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: admin/NTMakefile
 admin/ktutil-version.rc
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: appl/*
Copyright: 1995-2017, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: appl/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: appl/afsutil/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: appl/dbutils/*
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: appl/dceutils/*
Copyright: 1995, HEWLETT-PACKARD COMPANY
License: MIT~OSF

Files: appl/dceutils/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: appl/gssmask/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: appl/kf/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: appl/otp/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: appl/su/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: appl/test/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: cf/*
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: cf/maybe-valgrind.sh
Copyright: 1995-2016, Kungliga Tekniska Högskolan
License: BSD-3-clause

Files: cf/pkg.m4
Copyright: 2004, Scott James Remnant <scott@netsplit.com>.
License: GPL-2+ with Autoconf-data exception

Files: cf/symbol-version.py
Copyright: 1995-2017, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: debian/*
Copyright: 1999-2017 Brian May <bam@debian.org>
  2017 Dominik George <nik@naturalnet.de>
License: BSD-3-clause

Files: debian/missing-sources/*
Copyright: 2011, John Resig
License: Expat

Files: doc/*
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: doc/copyright.texi
Copyright: 2009, Apple Inc.
 1997-2011, Kungliga Tekniska HÃ¶gskolan
License: BSD-2-clause and/or BSD-3-clause and/or NTP

Files: doc/hx509.texi
Copyright: 1994-2008, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause and/or Expat

Files: doc/mdate-sh
Copyright: 1995-1997, Free Software Foundation, Inc.
License: GPL-2+

Files: etc/*
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: include/*
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: include/bits.c
Copyright: 2009-2013, Apple Inc.
 1997-2016, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: include/heim_threads.h
Copyright: 1995-2017, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: kadmin/*
Copyright: 1995-2017, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: kadmin/NTMakefile
 kadmin/kadmin-version.rc
 kadmin/kadmind-version.rc
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: kadmin/init.c
 kadmin/stash.c
Copyright: 2009-2013, Apple Inc.
 1997-2016, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: kcm/*
Copyright: 2009, Apple Inc.
 2005, PADL Software Pty Ltd.
License: BSD-3-clause

Files: kcm/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: kcm/acquire.c
 kcm/events.c
 kcm/glue.c
 kcm/headers.h
 kcm/renew.c
Copyright: 2003-2005, 2010, 2011, PADL Software Pty Ltd.
License: BSD-3-clause

Files: kcm/connect.c
Copyright: 2009-2013, Apple Inc.
 1997-2016, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: kcm/kcm.8
 kcm/log.c
 kcm/main.c
Copyright: 1995-2017, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: kcm/sessions.c
Copyright: 2009, Kungliga Tekniska Högskolan
 2009, Apple Inc.
License: BSD-3-clause

Files: kdc/*
Copyright: 1995-2017, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: kdc/NTMakefile
 kdc/hprop-version.rc
 kdc/hpropd-version.rc
 kdc/kdc-version.rc
 kdc/kstash-version.rc
 kdc/libkdc-version.rc
 kdc/string2key-version.rc
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: kdc/announce.c
Copyright: 2008, Apple Inc.
License: NTP

Files: kdc/config.c
 kdc/default_config.c
 kdc/fast.c
 kdc/kdc-tester.c
 kdc/log.c
 kdc/main.c
 kdc/pkinit.c
 kdc/set_dbinfo.c
Copyright: 2009-2013, Apple Inc.
 1997-2016, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: kdc/digest-service.c
Copyright: 2009, 2010, Apple Inc.
 1997-2007, Kungliga Tekniska Högskolan
License: BSD-3-clause

Files: kdc/kdc.h
Copyright: 2005, Andrew Bartlett <abartlet@samba.org>
 1997-2003, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: kdc/pkinit-ec.c
Copyright: 2009-2016, Kungliga Tekniska HÃ¶gskolan
 2009, 2010, Apple Inc.
License: BSD-3-clause

Files: kpasswd/*
Copyright: 1995-2017, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: kpasswd/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: kuser/*
Copyright: 1995-2017, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: kuser/NTMakefile
 kuser/heimtools-version.rc
 kuser/kdestroy-version.rc
 kuser/kdigest-version.rc
 kuser/kgetcred-version.rc
 kuser/kimpersonate-version.rc
 kuser/kinit-version.rc
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: kuser/heimtools-commands.in
Copyright: no-info-found
License: BSD-3-clause

Files: kuser/kinit.c
 kuser/klist.c
Copyright: 2009-2013, Apple Inc.
 1997-2016, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: kuser/kvno.c
Copyright: 1998, the FundsXpress, INC.
License: NTP

Files: lib/*
Copyright: 1995-2017, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: lib/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/asn1/NTMakefile
 lib/asn1/asn1_compile-version.rc
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/asn1/asn1-template.h
 lib/asn1/gen_template.c
Copyright: 2009, 2010, Apple Inc.
 1997-2007, Kungliga Tekniska Högskolan
License: BSD-3-clause

Files: lib/asn1/asn1_print.c
 lib/asn1/asn1parse.y
 lib/asn1/check-common.c
 lib/asn1/check-common.h
 lib/asn1/check-der.c
 lib/asn1/check-gen.c
 lib/asn1/check-template.c
 lib/asn1/der_copy.c
 lib/asn1/der_free.c
 lib/asn1/der_length.c
 lib/asn1/extra.c
 lib/asn1/gen.c
 lib/asn1/gen_glue.c
Copyright: 2009-2013, Apple Inc.
 1997-2016, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: lib/asn1/asn1_queue.h
Copyright: 1983, 1985, 1987, 1989-1994, The Regents of the University of California.
License: BSD-3-clause

Files: lib/asn1/fuzzer.c
 lib/asn1/template.c
Copyright: 2009-2016, Kungliga Tekniska HÃ¶gskolan
 2009, 2010, Apple Inc.
License: BSD-3-clause

Files: lib/base/*
Copyright: 2009-2016, Kungliga Tekniska HÃ¶gskolan
 2009, 2010, Apple Inc.
License: BSD-3-clause

Files: lib/base/NTMakefile
 lib/base/bsearch.c
 lib/base/db.c
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/base/baselocl.h
 lib/base/dict.c
Copyright: 2009-2013, Apple Inc.
 1997-2016, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: lib/base/data.c
 lib/base/roken_rename.h
Copyright: 1995-2017, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: lib/base/dll.c
Copyright: 2016, Kungliga Tekniska HÃ¶gskolan
License: BSD-2-clause

Files: lib/base/heimqueue.h
Copyright: 1983, 1985, 1987, 1989-1994, The Regents of the University of California.
License: BSD-3-clause

Files: lib/com_err/NTMakefile
 lib/com_err/compile_et-version.rc
 lib/com_err/libcom_err-version.rc
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/gssapi/NTMakefile
 lib/gssapi/libgssapi-version.rc
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/gssapi/gssapi_mech.h
Copyright: 2005, Doug Rabson
License: BSD-2-clause

Files: lib/gssapi/gsstool.c
Copyright: 2009-2013, Apple Inc.
 1997-2016, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: lib/gssapi/krb5/authorize_localname.c
 lib/gssapi/krb5/cfx.c
 lib/gssapi/krb5/cfx.h
 lib/gssapi/krb5/inquire_cred_by_oid.c
 lib/gssapi/krb5/inquire_sec_context_by_oid.c
 lib/gssapi/krb5/pname_to_uid.c
 lib/gssapi/krb5/set_cred_option.c
 lib/gssapi/krb5/set_sec_context_option.c
Copyright: 2003-2005, 2010, 2011, PADL Software Pty Ltd.
License: BSD-3-clause

Files: lib/gssapi/mech/*
Copyright: 2005, Doug Rabson
License: BSD-2-clause

Files: lib/gssapi/mech/compat.h
 lib/gssapi/mech/gss_acquire_cred_with_password.c
 lib/gssapi/mech/gss_authorize_localname.c
 lib/gssapi/mech/gss_buffer_set.c
 lib/gssapi/mech/gss_delete_name_attribute.c
 lib/gssapi/mech/gss_display_name_ext.c
 lib/gssapi/mech/gss_export_name_composite.c
 lib/gssapi/mech/gss_get_name_attribute.c
 lib/gssapi/mech/gss_inquire_cred_by_oid.c
 lib/gssapi/mech/gss_inquire_name.c
 lib/gssapi/mech/gss_inquire_sec_context_by_oid.c
 lib/gssapi/mech/gss_pname_to_uid.c
 lib/gssapi/mech/gss_set_cred_option.c
 lib/gssapi/mech/gss_set_name_attribute.c
 lib/gssapi/mech/gss_set_sec_context_option.c
Copyright: 2003-2005, 2010, 2011, PADL Software Pty Ltd.
License: BSD-3-clause

Files: lib/gssapi/mech/doxygen.c
 lib/gssapi/mech/gss_add_oid_set_member.c
 lib/gssapi/mech/gss_cred.c
 lib/gssapi/mech/gss_decapsulate_token.c
 lib/gssapi/mech/gss_duplicate_oid.c
 lib/gssapi/mech/gss_encapsulate_token.c
 lib/gssapi/mech/gss_oid_equal.c
 lib/gssapi/mech/gss_oid_to_str.c
 lib/gssapi/mech/gss_pseudo_random.c
 lib/gssapi/mech/gss_release_oid.c
 lib/gssapi/mech/mech_locl.h
Copyright: 1995-2017, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: lib/gssapi/mech/gss_acquire_cred_ext.c
Copyright: 2011, PADL Software Pty Ltd.
 2005, Doug Rabson
License: BSD-2-clause

Files: lib/gssapi/mech/gss_destroy_cred.c
Copyright: 2009, Apple Inc.
 2005, Doug Rabson
License: BSD-2-clause

Files: lib/gssapi/mech/gss_display_status.c
Copyright: 2005, Doug Rabson
License: BSD-2-clause and/or BSD-3-clause

Files: lib/gssapi/mech/gss_mo.c
Copyright: 2010, PADL Software Pty Ltd.
 2010, Kungliga Tekniska HÃ¶gskolan
 2010, Apple Inc.
License: BSD-3-clause

Files: lib/gssapi/mech/gss_store_cred.c
Copyright: 1995-2016, Kungliga Tekniska Högskolan
License: BSD-3-clause

Files: lib/gssapi/mech/mechqueue.h
Copyright: 1983, 1985, 1987, 1989-1994, The Regents of the University of California.
License: BSD-3-clause

Files: lib/gssapi/netlogon/*
Copyright: 2009-2016, Kungliga Tekniska HÃ¶gskolan
 2009, 2010, Apple Inc.
License: BSD-3-clause

Files: lib/gssapi/netlogon/accept_sec_context.c
 lib/gssapi/netlogon/canonicalize_name.c
 lib/gssapi/netlogon/context_time.c
 lib/gssapi/netlogon/display_status.c
 lib/gssapi/netlogon/export_name.c
 lib/gssapi/netlogon/iter_cred.c
 lib/gssapi/netlogon/process_context_token.c
Copyright: 2009, Kungliga Tekniska Högskolan
 2009, Apple Inc.
License: BSD-3-clause

Files: lib/gssapi/ntlm/creds.c
Copyright: 2009-2013, Apple Inc.
 1997-2016, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: lib/gssapi/ntlm/inquire_sec_context_by_oid.c
 lib/gssapi/ntlm/iter_cred.c
Copyright: 2009, 2010, Apple Inc.
 1997-2007, Kungliga Tekniska Högskolan
License: BSD-3-clause

Files: lib/gssapi/spnego/*
Copyright: 2003-2005, 2010, 2011, PADL Software Pty Ltd.
License: BSD-3-clause

Files: lib/gssapi/spnego/accept_sec_context.c
 lib/gssapi/spnego/init_sec_context.c
Copyright: 2004, PADL Software Pty Ltd.
 1997-2006, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: lib/gssapi/test_add_store_cred.c
Copyright: 2015, Cryptonector LLC.
License: BSD-3-clause

Files: lib/hcrypto/NTMakefile
 lib/hcrypto/evp-pkcs11.c
 lib/hcrypto/evp-pkcs11.h
 lib/hcrypto/evp-w32.c
 lib/hcrypto/evp-w32.h
 lib/hcrypto/evp-wincng.c
 lib/hcrypto/evp-wincng.h
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/hcrypto/common.c
 lib/hcrypto/common.h
Copyright: 2009-2016, Kungliga Tekniska HÃ¶gskolan
 2009, 2010, Apple Inc.
License: BSD-3-clause

Files: lib/hcrypto/ec.c
 lib/hcrypto/ec.h
 lib/hcrypto/ecdh.h
 lib/hcrypto/ecdsa.h
Copyright: 1995-2016, Kungliga Tekniska Högskolan
License: BSD-3-clause

Files: lib/hcrypto/evp-cc.c
 lib/hcrypto/rand.c
 lib/hcrypto/test_rand.c
Copyright: 2009-2013, Apple Inc.
 1997-2016, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: lib/hcrypto/evp-openssl.c
Copyright: 2016, Kungliga Tekniska HÃ¶gskolan
License: BSD-2-clause

Files: lib/hcrypto/libtommath/*
Copyright: no-info-found
License: public-domain

Files: lib/hcrypto/libtommath/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/hcrypto/rand-fortuna.c
Copyright: 2005, Marko Kreen
License: BSD-2-clause

Files: lib/hcrypto/rijndael-alg-fst.c
 lib/hcrypto/rijndael-alg-fst.h
Copyright: no-info-found
License: public-domain

Files: lib/hcrypto/test_dh.c
Copyright: 2007, Novell, Inc.
License: BSD-3-clause

Files: lib/hdb/NTMakefile
 lib/hdb/libhdb-version.rc
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/hdb/hdb-keytab.c
Copyright: 2009, Kungliga Tekniska Högskolan
 2009, Apple Inc.
License: BSD-3-clause

Files: lib/hdb/hdb-ldap.c
Copyright: 2015, Timothy Pearson.
 2004, Andrew Bartlett.
 2003-2008, Kungliga Tekniska HÃ¶gskolan.
 1999-2001, 2003, PADL Software Pty Ltd.
License: BSD-3-clause

Files: lib/hdb/hdb-mdb.c
Copyright: 2011, Howard Chu, Symas Corp.
 1997-2006, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: lib/hdb/hdb-mitdb.c
 lib/hdb/hdb.c
Copyright: 2009-2013, Apple Inc.
 1997-2016, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: lib/hdb/hdb-sqlite.c
Copyright: 1995-2016, Kungliga Tekniska Högskolan
License: BSD-3-clause

Files: lib/hdb/test_hdbplugin.c
Copyright: 2013, Jeffrey Clark
License: BSD-3-clause

Files: lib/heimdal/*
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/hx509/NTMakefile
 lib/hx509/hxtool-version.rc
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/hx509/data/*
Copyright: 1995-2016, Kungliga Tekniska Högskolan
License: BSD-3-clause

Files: lib/hx509/keyset.c
 lib/hx509/peer.c
Copyright: 2009-2013, Apple Inc.
 1997-2016, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: lib/ipc/*
Copyright: 2009, Kungliga Tekniska Högskolan
 2009, Apple Inc.
License: BSD-3-clause

Files: lib/ipc/server.c
Copyright: 2009, Kungliga Tekniska Hï¿½gskolan
 2009, Apple Inc.
License: BSD-3-clause

Files: lib/kadm5/NTMakefile
 lib/kadm5/iprop-log-version.rc
 lib/kadm5/ipropd-master-version.rc
 lib/kadm5/ipropd-slave-version.rc
 lib/kadm5/libkadm5srv-version.rc
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/kadm5/ent_setup.c
Copyright: 2009-2013, Apple Inc.
 1997-2016, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: lib/kafs/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/kafs/rxkad_kdf.c
Copyright: 2013, the Massachusetts Institute of Technology
 2013, 2014, Carnegie Mellon University
 1995-2003, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: lib/kdfs/*
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/krb5/NTMakefile
 lib/krb5/ccache_plugin.h
 lib/krb5/config_reg.c
 lib/krb5/db_plugin.h
 lib/krb5/dll.c
 lib/krb5/expand_path.c
 lib/krb5/kuserok_plugin.h
 lib/krb5/pcache.c
 lib/krb5/sp800-108-kdf.c
 lib/krb5/test_canon.c
 lib/krb5/test_set_kvno0.c
 lib/krb5/verify_krb5_conf-version.rc
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/krb5/acache.c
 lib/krb5/cache.c
 lib/krb5/config_file.c
 lib/krb5/constants.c
 lib/krb5/context.c
 lib/krb5/dcache.c
 lib/krb5/fcache.c
 lib/krb5/get_cred.c
 lib/krb5/init_creds.c
 lib/krb5/init_creds_pw.c
 lib/krb5/krb5.h
 lib/krb5/krb5_locl.h
 lib/krb5/krbhst.c
 lib/krb5/locate_plugin.h
 lib/krb5/log.c
 lib/krb5/mcache.c
 lib/krb5/pkinit.c
 lib/krb5/send_to_kdc.c
 lib/krb5/ticket.c
Copyright: 2009-2013, Apple Inc.
 1997-2016, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: lib/krb5/deprecated.c
 lib/krb5/test_gic.c
Copyright: 1995-2016, Kungliga Tekniska Högskolan
License: BSD-3-clause

Files: lib/krb5/kcm.c
 lib/krb5/kcm.h
Copyright: 2009, Apple Inc.
 2005, PADL Software Pty Ltd.
License: BSD-3-clause

Files: lib/krb5/pkinit-ec.c
Copyright: 2009-2016, Kungliga Tekniska HÃ¶gskolan
 2009, 2010, Apple Inc.
License: BSD-3-clause

Files: lib/krb5/test_config_strings.cfg
Copyright: no-info-found
License: BSD-3-clause

Files: lib/libedit/*
Copyright: 1983, 1985, 1987, 1989-1994, The Regents of the University of California.
License: BSD-3-clause

Files: lib/libedit/doc/*
Copyright: 1997-2014, The NetBSD Foundation, Inc.
License: BSD-2-clause

Files: lib/libedit/doc/mdoc2man.awk
Copyright: 2003, Peter Stuge <stuge-mdoc2man@cdy.org>
License: ISC

Files: lib/libedit/src/chartype.c
 lib/libedit/src/chartype.h
 lib/libedit/src/editline.3
 lib/libedit/src/editrc.5
 lib/libedit/src/eln.c
Copyright: 1997-2014, The NetBSD Foundation, Inc.
License: BSD-2-clause

Files: lib/libedit/src/editline.7
Copyright: 2016, Ingo Schwarze <schwarze@openbsd.org>
License: ISC

Files: lib/libedit/src/filecomplete.c
 lib/libedit/src/filecomplete.h
 lib/libedit/src/read.h
 lib/libedit/src/readline.c
Copyright: 1997, 2001, The NetBSD Foundation, Inc.
License: BSD-2-Clause-NetBSD and/or BSD-2-clause

Files: lib/libedit/src/readline/*
Copyright: 1997, 2001, The NetBSD Foundation, Inc.
License: BSD-2-Clause-NetBSD and/or BSD-2-clause

Files: lib/libedit/src/vis.c
Copyright: 1982, 1986, 1988, 1989, 1993, The Regents of the University of California.
License: BSD-2-clause and/or BSD-3-clause

Files: lib/ntlm/*
Copyright: 2009-2013, Apple Inc.
 1997-2016, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: lib/ntlm/NTMakefile
 lib/ntlm/libheimntlm-version.rc
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/ntlm/apop.c
Copyright: 2009-2016, Kungliga Tekniska HÃ¶gskolan
 2009, 2010, Apple Inc.
License: BSD-3-clause

Files: lib/ntlm/heimntlm.h
 lib/ntlm/test_ntlm.c
Copyright: 1995-2017, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: lib/otp/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/roken/NTMakefile
 lib/roken/dirent-test.c
 lib/roken/dirent.c
 lib/roken/dirent.hin
 lib/roken/dlfcn.hin
 lib/roken/dlfcn_w32.c
 lib/roken/getifaddrs_w32.c
 lib/roken/rename.c
 lib/roken/simple_exec_w32.c
 lib/roken/sleep.c
 lib/roken/sockstartup_w32.c
 lib/roken/syslogc.c
 lib/roken/test-mini_inetd.c
 lib/roken/win32_alloc.c
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/roken/daemon.c
 lib/roken/fnmatch.c
 lib/roken/fnmatch.hin
 lib/roken/getcap.c
 lib/roken/getopt.c
 lib/roken/getusershell.c
 lib/roken/glob.c
 lib/roken/glob.hin
 lib/roken/iruserok.c
 lib/roken/qsort.c
 lib/roken/unvis.c
 lib/roken/vis.hin
Copyright: 1983, 1985, 1987, 1989-1994, The Regents of the University of California.
License: BSD-3-clause

Files: lib/roken/getifaddrs-test.c
Copyright: 2009, Secure Endpoints Inc.
 2009, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: lib/roken/install-sh
Copyright: 1991, the Massachusetts Institute of Technology
License: HPND-sell-variant

Files: lib/roken/memset_s.c
Copyright: 2015, Your File System Inc.
License: BSD-2-clause

Files: lib/roken/missing
Copyright: 1995-1997, Free Software Foundation, Inc.
License: GPL-2+

Files: lib/roken/socket_wrapper.c
 lib/roken/socket_wrapper.h
Copyright: Stefan Metzmacher 2006, <metze@samba.org>
 Jelmer Vernooij 2005, <jelmer@samba.org>
License: BSD-3-clause

Files: lib/roken/strtoll.c
 lib/roken/strtoull.c
Copyright: 2011, The FreeBSD Foundation
 1992, 1993, The Regents of the University of California.
License: BSD-3-clause

Files: lib/roken/syslog.hin
 lib/roken/vis.c
Copyright: 1982, 1986, 1988, 1989, 1993, The Regents of the University of California.
License: BSD-2-clause and/or BSD-3-clause

Files: lib/roken/test-detach.c
Copyright: 2015, Cryptonector LLC
License: BSD-2-clause

Files: lib/sl/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/sqlite/*
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/vers/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: lib/wind/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: packages/*
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: packages/windows/installer/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: tests/*
Copyright: 1995-2017, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: tests/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: tests/can/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: tests/db/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: tests/gss/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: tests/java/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: tests/kdc/NTMakefile
 tests/kdc/check-canon.in
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: tests/ldap/*
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: tests/ldap/check-ldap.in
Copyright: 1995-2017, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: tests/plugin/*
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: tests/plugin/check-pac.in
Copyright: 1995-2017, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: tools/*
Copyright: 1995-2017, Kungliga Tekniska HÃ¶gskolan
License: BSD-3-clause

Files: tools/NTMakefile
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: windows/*
Copyright: 2009-2017, Secure Endpoints Inc.
License: BSD-2-clause

Files: windows/NTMakefile.sdk
Copyright: 2021, PADL Software Pty Ltd.
License: BSD-2-clause

Files: doc/layman.asc lib/hcrypto/camellia-ntt.c lib/hcrypto/camellia-ntt.h lib/hcrypto/libtommath/bn_mp_n_root.c lib/hcrypto/libtommath/mtest/* lib/hcrypto/libtommath/pics/* lib/hcrypto/libtommath/pics/design_process.tif lib/hcrypto/libtommath/pics/expt_state.sxd lib/hcrypto/libtommath/pics/expt_state.tif lib/hcrypto/libtommath/pics/primality.tif lib/hcrypto/libtommath/pics/radix.sxd lib/hcrypto/libtommath/pics/sliding_window.sxd lib/hcrypto/libtommath/pics/sliding_window.tif lib/hcrypto/passwd_dialog.aps lib/hcrypto/passwd_dialog.rc lib/hcrypto/rsakey.der lib/hcrypto/rsakey2048.der lib/hcrypto/rsakey4096.der lib/hx509/ChangeLog lib/hx509/data/PKITS_data.zip lib/hx509/data/crl1.der lib/hx509/data/key.der lib/hx509/data/key2.der lib/hx509/data/ocsp-resp1-2.der lib/hx509/data/ocsp-resp1-ca.der lib/hx509/data/ocsp-resp1-keyhash.der lib/hx509/data/ocsp-resp1-ocsp-no-cert.der lib/hx509/data/ocsp-resp1-ocsp.der lib/hx509/data/ocsp-resp2.der lib/hx509/data/sub-cert.p12 lib/hx509/data/test-enveloped-aes-128 lib/hx509/data/test-enveloped-aes-256 lib/hx509/data/test-enveloped-des lib/hx509/data/test-enveloped-des-ede3 lib/hx509/data/test-enveloped-rc2-128 lib/hx509/data/test-enveloped-rc2-40 lib/hx509/data/test-enveloped-rc2-64 lib/hx509/data/test-nopw.p12 lib/hx509/data/test.p12 lib/hx509/ref/* lib/libedit/INSTALL lib/libedit/src/wcsdup.c lib/roken/detach.c lib/wind/DerivedNormalizationProps.txt packages/windows/installer/* tests/can/heim-0.8.req tests/kdc/hdb-mitdb windows/NTMakefile.version
Copyright: 1996-2017 Kungliga Tekniska Högskolan
 2009, 2010 Apple, Inc.
 1991, 2013 Massachusetts Institute of Technology
 1995-1997 Eric Young <eay@@mincom.oz.au>
 1988, 1990, 1993 The Regents of the University of California
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 3. Neither the name of the Institute nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

Files: lib/wind/rfc3492.txt
Copyright: 2003 The Internet Society
License: custom
 Regarding this entire document or any portion of it (including the
 pseudocode and C code), the author makes no guarantees and is not
 responsible for any damage resulting from its use.  The author grants
 irrevocable permission to anyone to use, modify, and distribute it in
 any way that does not diminish the rights of anyone else to use,
 modify, and distribute it, provided that redistributed derivative
 works do not contain misleading author or version information.
 Derivative works need not be licensed under similar terms.
